package vfs

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/hanwen/go-fuse/v2/fs"
	"github.com/hanwen/go-fuse/v2/fuse"
	vault "github.com/hashicorp/vault/api"
)

type vfsRoot struct {
	fs.Inode
	client *vault.Client
	config *vault.Config
	path   string
	store  string
}

func (vr *vfsRoot) onAddDiver(ctx context.Context, path string) ([]string, error) {
	if !strings.HasSuffix(path, "/") {
		path = fmt.Sprintf("%s/", path)
	}

	resp, err := vr.client.Logical().ListWithContext(ctx, fmt.Sprintf("%s/metadata/%s", vr.store, path))
	if err != nil {
		return nil, err
	}
	if resp == nil {
		// This should only ever happen when trying to query the root of an empty secret store
		return nil, fmt.Errorf("no metadata found for %s/%s\n", vr.store, path)
	}

	raw, ok := resp.Data["keys"].([]interface{})
	if !ok {
		return nil, errors.New(fmt.Sprintf("value type assertion failed: %T %#v\n", resp.Data["keys"], resp.Data["keys"]))
	}

	var res []string
	for i := range raw {
		key, ok := raw[i].(string)
		if !ok {
			return nil, fmt.Errorf("value type assertion failed: %T %#v\n", raw[i], raw[i])
		}

		if strings.HasSuffix(key, "/") {
			out, err := vr.onAddDiver(ctx, fmt.Sprintf("%s%s", path, key))
			if err != nil || out == nil {
				log.Printf("failed to process %s/metadata/%s/%s: %v\n", vr.store, path, key, err)
			} else if len(out) > 0 {
				res = append(res, out...)
			}
		} else {
			res = append(res, fmt.Sprintf("%s%s", path, key))
		}
	}

	return res, nil
}

func (vr *vfsRoot) OnAdd(ctx context.Context) {
	// Doing secret discovery
	secrets, err := vr.onAddDiver(ctx, vr.path)
	if err != nil {
		log.Fatalf("failed to explore %s/%s: %v\n", vr.store, vr.path, err)
	}

	if secrets == nil || len(secrets) == 0 {
		log.Fatalf("failed to find secrets at %s/%s", vr.store, vr.path)
	}

	// Makin' nodes
	for _, secret := range secrets {
		dir, base := filepath.Split(secret)

		p := &vr.Inode
		for _, component := range strings.Split(dir, "/") {
			if len(component) == 0 {
				continue
			}

			ch := p.GetChild(component)
			if ch == nil {
				// No child found -- make one
				ch = p.NewPersistentInode(
					ctx,
					&fs.Inode{},
					fs.StableAttr{Mode: fuse.S_IFDIR},
				)
				p.AddChild(component, ch, true)
			}

			p = ch
		}

		node, err := newVaultSecret(ctx, secret, vr)
		if err != nil {
			log.Fatalf("unable to create secret node %s/%s: %v\n", vr.store, secret, err)
		}

		ch := p.NewPersistentInode(ctx, node, fs.StableAttr{})
		p.AddChild(base, ch, true)
	}
}

func NewVaultFileSystem(store, path string) (fs.InodeEmbedder, error) {
	out := &vfsRoot{store: store, path: path}

	out.config = vault.DefaultConfig()
	if err := out.config.ReadEnvironment(); err != nil {
		return nil, err
	}

	var err error
	out.client, err = vault.NewClient(out.config)
	if err != nil {
		return nil, err
	}

	if out.client.Token() == "" {
		home, err := os.UserHomeDir()
		if err != nil {
			return nil, err
		}

		_, err = os.Stat(home)
		if err != nil {
			return nil, err
		}

		fp, err := ioutil.ReadFile(filepath.Join(home, ".vault-token"))
		if err != nil {
			return nil, err
		}

		if fp == nil || len(fp) == 0 {
			return nil, fmt.Errorf("no vault token found in ~/.vault-token")
		}

		out.client.SetToken((string)(fp))
	}

	return out, nil
}
