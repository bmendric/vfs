package vfs

import (
	"context"
	"encoding/json"
	"log"
	"syscall"
	"time"

	"github.com/hanwen/go-fuse/v2/fs"
	"github.com/hanwen/go-fuse/v2/fuse"
)

const bs = 512

type vfsSecret struct {
	fs.Inode
	root *vfsRoot

	path  string
	data  []byte
	mtime time.Time
}

func (vs *vfsSecret) Getattr(ctx context.Context, f fs.FileHandle, out *fuse.AttrOut) syscall.Errno {
	out.Mode = 0400
	out.Nlink = 1
	out.Mtime = (uint64)(vs.mtime.Unix())
	out.Atime = out.Mtime
	out.Ctime = out.Mtime
	out.Size = (uint64)(len(vs.data))
	out.Blksize = bs
	out.Blocks = (out.Size + bs - 1) / bs
	return 0
}

func (vs *vfsSecret) Open(ctx context.Context, flags uint32) (fs.FileHandle, uint32, syscall.Errno) {
	// We are already aware of the contents of the secret when we create the
	// vfsSecret object and we don't need ot return a real filehandle, so hint
	// the kernel to cache the data
	return nil, fuse.FOPEN_KEEP_CACHE, 0
}

func (vs *vfsSecret) Read(ctx context.Context, f fs.FileHandle, dest []byte, off int64) (fuse.ReadResult, syscall.Errno) {
	end := int(off) + len(dest)
	if end > len(vs.data) {
		end = len(vs.data)
	}
	return fuse.ReadResultData(vs.data[off:end]), 0
}

func newVaultSecret(ctx context.Context, path string, root *vfsRoot) (*vfsSecret, error) {
	out := &vfsSecret{path: path, root: root}

	meta, err := out.root.client.KVv2(out.root.store).GetMetadata(ctx, out.path)
	if err != nil {
		return nil, err
	}

	out.mtime = meta.UpdatedTime

	resp, err := out.root.client.KVv2(out.root.store).Get(ctx, out.path)
	if err != nil {
		log.Fatalf("unable to retrieve secret %s/%s: %v\n", out.root.store, out.path, err)
	}

	out.data, err = json.Marshal(resp.Data)
	if err != nil {
		log.Fatalf("failed to marshall response to json: %v\n", err)
	}

	// Not having a new line drives me nuts -- maybe expose this as an option?
	out.data = append(out.data, 0x0A)

	return out, nil
}
