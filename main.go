// Driver program for gitlab.com/bmendric/vfs, a filesystem for mounting HashiCorp Vault KV secrets
package main

import (
	"fmt"
	"log"
	"os"

	"github.com/hanwen/go-fuse/v2/fs"
	"github.com/spf13/pflag"
	"gitlab.com/bmendric/vfs/internal/vfs"
)

func main() {
	// Argument processing
	debug := pflag.BoolP("debug", "d", false, "print debug messages")
	store := pflag.StringP("store", "s", "secret-test", "KV-V2 secret store name")
	path := pflag.StringP("path", "p", "", "path within the store to mount")
	pflag.Parse()
	if pflag.NArg() < 1 {
		fmt.Fprintf(os.Stderr, "usage: %s MOUNTPOINT\n", os.Args[0])
		os.Exit(-1)
	}

	opts := &fs.Options{}
	opts.Debug = *debug

	root, err := vfs.NewVaultFileSystem(*store, *path)
	if err != nil {
		log.Fatalln(err)
	}

	server, err := fs.Mount(pflag.Arg(0), root, opts)
	if err != nil {
		log.Fatalf("mount failed: %v\n", err)
	}
	server.Wait()
}
