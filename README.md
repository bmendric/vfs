# vfs

HashiCorp Vault Filesystem

## Use

Don't use this -- I haven't thought about it much, but this just generally seems like a bad idea and not something you actually would want to use.

It is just a silly thing I made for funzies.

### But actually, how do I use it?

You really want to use this thing? Here you go --

1. Clone the project
1. `go build`
1. Create a temporary directly `tmpdir=$(mktemp -d)`
1. Configure your Vault environment
    - The Vault client within `vfs` will attempt to read Vault configuration from your environment. The main thing you need to set is `VAULT_ADDR`
    - Your Vault token will either be read out of `VAULT_TOKEN` environment variable, or read from `~/.vault-token`
1. Create the mount point `vfs $tmpdir`
    - You can configure where `vfs` will look for secrets using the `--path` and `--store` flag. See the command's help documentation for more information
1. When you are done, `fusermount -u $tmpdir`

## Supported Features

- [ ] Access
- [ ] Allocate
- [ ] CopyFileRange
- [ ] Create
- [ ] Flush
- [ ] Fsync
- [x] Getattr
- [ ] Getlk
- [ ] Getxattr
- [ ] Link
- [ ] Listxattr
- [ ] Lookup
- [ ] Lseek
- [ ] Mkdir
- [ ] Mknod
- [x] OnAdd
- [ ] Opendir
- [x] Open
- [ ] Readdir
- [x] Read
- [ ] Readlink
- [ ] Release
- [ ] Removexattr
- [ ] Rename
- [ ] Rmdir
- [ ] Setattr
- [ ] Setlk
- [ ] Setlkw
- [ ] Setxattr
- [ ] Statfs
- [ ] Symlink
- [ ] Unlink
- [ ] Write

I do not actually understand file systems/FUSE enough to know which of these actually makes sense or would be applicable here. I think the main ones I would be interested in implementing in the future are write, rename, create, and mkdir.

## Why

<table border="0">
  <tr>
    <td><img src="images/gary.png"  width="300"></td>
    <td>Gary texted me late on a Thursday with a "great idea". It stuck with me for some reason, so I made it a thing. This is Gary's fault.</td>
  </tr>
</table>
