#!/bin/bash

# Run a Vault dev server with the following command
#   vault server -dev -dev-root-token-id jeff
# Run this file from within the gitlab.com/bmendric/vfs/test folder

set -eux

export VAULT_ADDR=http://localhost:8200
export VAULT_TOKEN=jeff

vault secrets enable -path=secret-test kv-v2

fd -t f -e json -j 1 -x vault kv put -mount=secret-test {.} @{}
